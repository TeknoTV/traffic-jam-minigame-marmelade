using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectCar : MonoBehaviour
{
    [SerializeField] private PlayerManager playerManager;
    [SerializeField] private List<GameObject> cars;
    [SerializeField] Transform carsObject;
    [SerializeField] float rotationSpeed = 1;
    private int selectedCar = 0;
    Vector3 rotationVector = new Vector3(0, 1, 0);

    private GameState gameState;

    void Awake()
    {
        GameManager.OnGameStateChanged += GameManagerOnGameStateChanged;
        selectedCar = Random.Range(0, cars.Count);
        SetSelectedCar();
    }

    void OnDestroy()
    {
        GameManager.OnGameStateChanged -= GameManagerOnGameStateChanged;
    }

    private void GameManagerOnGameStateChanged(GameState _gameState)
    {
        gameState = _gameState;
    }

    void FixedUpdate()
    {
        if(gameState == GameState.SelectCar)
        {
            carsObject.Rotate(rotationVector * rotationSpeed * Time.deltaTime);
        }
    }

    public void CycleCars(bool right)
    {
        if(right)
        {
            selectedCar++;
            if(selectedCar >= cars.Count)
            {
                selectedCar = 0;
            }
        }
        else
        {
            
            selectedCar--;
            if(selectedCar < 0)
            {
                selectedCar = cars.Count - 1;
            }
        }

        SetSelectedCar();
    }

    void SetSelectedCar()
    {
        for (int car = 0; car < cars.Count; car++)
        {
            cars[car].SetActive(car == selectedCar);
        }
        playerManager.SetSelectedCar(selectedCar);
    }
}
