using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    SelectCar = 0,
    Play,
    End
}


public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameState gameState;

    public static event Action<GameState> OnGameStateChanged;

    [SerializeField] private int gameDuration = 30;
    [SerializeField] private Transform carsPool;
    [SerializeField] private Transform moneyPool;
    float timer;
    public float Timer => timer;

    void Start()
    {
        UpdateGameState(GameState.SelectCar);
    }

    public void UpdateGameState(GameState newState)
    {
        gameState = newState;
        
        switch (newState)
        {
            case GameState.SelectCar:
                break;
            case GameState.Play:
                HandlePlay();
                break;
            case GameState.End:
                HandleEnd();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);

        }

        OnGameStateChanged?.Invoke(newState);
    }

    private void HandlePlay()
    {
        timer = gameDuration;
    }

    private void HandleEnd()
    {
        foreach (Transform child in carsPool) 
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in moneyPool) 
        {
            Destroy(child.gameObject);
        }
    }

    void FixedUpdate()
    {
        if(gameState == GameState.Play)
        {
            timer -= Time.deltaTime;
            if(timer < 0)
            {
                timer = 0;
                UpdateGameState(GameState.End);
            }
        }
    }

    public void StartGame()
    {
        UpdateGameState(GameState.Play);
    }

    public void RestartGame()
    {
        UpdateGameState(GameState.SelectCar);
    }
}
