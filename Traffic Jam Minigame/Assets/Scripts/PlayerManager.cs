using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CarColor
{
    Yellow = 0,
    Blue,
    Orange,
    Pink
}

public class PlayerManager : MonoBehaviour
{
    [SerializeField] List<GameObject> carsList;
    [SerializeField] private Transform carsPool;
    [SerializeField] private Transform moneyPool;
    [SerializeField] private HudManager hudManager; 
    [SerializeField] private Slider playerAmmountSlider; 
    private CarColor selectedCar = CarColor.Yellow;
    private int playerAmmount = 4;

    private int positionValue = 105;
    private GameState gameState;

    void Awake()
    {
        GameManager.OnGameStateChanged += GameManagerOnGameStateChanged;
        playerAmmount = Mathf.RoundToInt(playerAmmountSlider.value);
    }

    void OnDestroy()
    {
        GameManager.OnGameStateChanged -= GameManagerOnGameStateChanged;
    }

    private void GameManagerOnGameStateChanged(GameState _gameState)
    {
        gameState = _gameState;
        if(gameState == GameState.Play)
        {
            SpawnPlayerAndBots();
        }
    }

    void SpawnPlayerAndBots()
    {
        int selectedCarInt = (int)selectedCar;
        SpawnCar(selectedCarInt, true);
        if(selectedCarInt > playerAmmount)
        {
            playerAmmount--;
        }
        for (int i = 0; i < playerAmmount; i++)
        {
            if(i != selectedCarInt)
            {
                SpawnCar(i, false);
            }
        }
    }

    private void SpawnCar(int color, bool isPlayer)
    {
        GameObject car = Instantiate(carsList[color], GetCarPosition(color), GetCarRotation(color), carsPool);
        car.GetComponent<CarController>().StartCar(isPlayer ? CarType.Player : CarType.Bot, (CarColor)color, moneyPool, hudManager);
    }

    private Vector3 GetCarPosition(int color)
    {
        int x = color < 2 ? positionValue : -positionValue;
        int z = color == 0 || color == 3 ? positionValue : -positionValue;
        return new Vector3(x, 0, z);
    }

    
    private Quaternion GetCarRotation(int color)
    {
        return Quaternion.Euler(0, -135 + 90 * color, 0);
    }

    public void SetSelectedCar(int carColor)
    {
        selectedCar = (CarColor)carColor;
    }

    public void SetPlayerNumber(float value)
    {
        playerAmmount = Mathf.RoundToInt(value);
    }
}
