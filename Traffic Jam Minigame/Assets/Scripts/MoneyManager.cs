using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> moneyAssets;
    [SerializeField] private Transform moneyPool;
    private int spawnTime = 3;
    private Vector3 lastPosition;
    private static int maxValue = 110;
    private GameState gameState;

    void Awake()
    {
        GameManager.OnGameStateChanged += GameManagerOnGameStateChanged;
    }

    void OnDestroy()
    {
        GameManager.OnGameStateChanged -= GameManagerOnGameStateChanged;
    }

    private void GameManagerOnGameStateChanged(GameState _gameState)
    {
        gameState = _gameState;
        if(gameState == GameState.Play)
        {
            StartCoroutine(MoneySpawnLoop());
        }
    }

    private IEnumerator MoneySpawnLoop()
    {
        for(;;)
        {
            if(gameState != GameState.Play) break;
            for (int i = 0; i < Random.Range(10, 15); i++)
            {
                SpawnMoney();
            }
            yield return new WaitForSeconds(spawnTime);
        }
    }

    private void SpawnMoney()
    {
        Vector3 position = lastPosition;
        while(!IsValidPosition(position)) // prevent car to go to start position
        {
            lastPosition = GetRandomPosition();
        }
        GameObject go = Instantiate(moneyAssets[Random.Range(0, moneyAssets.Count)], position, GetRandomRotation(), moneyPool);
    }

    private bool IsValidPosition(Vector3 position)
    {
        return Vector3.Distance(position, lastPosition) > 5;
    }

    private Vector3 GetRandomPosition()
    {
        float x = Random.Range(-maxValue + 1, maxValue);
        float z = Random.Range(-maxValue + 1, maxValue);
        return new Vector3(x, 0, z);
    }

    private Quaternion GetRandomRotation()
    {
        return Random.Range(0, 100) < 50 ? Quaternion.Euler(-90, 90, 0) : Quaternion.Euler(-90, 0, 0);
    }

}
