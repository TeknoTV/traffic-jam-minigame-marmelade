using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("Select Car")]
    [SerializeField] private GameObject selectCarMenu;
    [SerializeField] private GameObject carOptions;
    [SerializeField] private GameObject instructions;
    [SerializeField] private GameObject options;

    [Header("HUD")]
    [SerializeField] private GameObject hud;

    [Header("End")]
    [SerializeField] private GameObject end;
    private GameState gameState;

    void Awake()
    {
        GameManager.OnGameStateChanged += GameManagerOnGameStateChanged;
    }

    void OnDestroy()
    {
        GameManager.OnGameStateChanged -= GameManagerOnGameStateChanged;
    }

    private void GameManagerOnGameStateChanged(GameState _gameState)
    {
        gameState = _gameState;

        selectCarMenu.SetActive(gameState == GameState.SelectCar);
        carOptions.SetActive(gameState == GameState.SelectCar);
        hud.SetActive(gameState == GameState.Play || gameState == GameState.End);
        end.SetActive(gameState == GameState.End);
    }

    public void ShowInstructions(bool show)
    {
        instructions.SetActive(show);
    }

    public void ShowOptions(bool show)
    {
        options.SetActive(show);
    }
}
