using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcManager : MonoBehaviour
{
    [SerializeField] private GameObject NpcAsset;
    [SerializeField] private Transform carsPool;
    private int spawnTime = 5;
    private Vector3 lastPosition;
    private static int laneValue = 15;
    private static int quadrantValue = 150;
    private GameState gameState;

    void Awake()
    {
        GameManager.OnGameStateChanged += GameManagerOnGameStateChanged;
    }

    void OnDestroy()
    {
        GameManager.OnGameStateChanged -= GameManagerOnGameStateChanged;
    }

    private void GameManagerOnGameStateChanged(GameState _gameState)
    {
        gameState = _gameState;
        if(gameState == GameState.Play)
        {
            StartCoroutine(NpcSpawnLoop());
        }
    }

    private IEnumerator NpcSpawnLoop()
    {
        for(;;)
        {
            if(gameState != GameState.Play) break;
            SpawnNpc();
            yield return new WaitForSeconds(1);
            SpawnNpc();
            yield return new WaitForSeconds(spawnTime);
        }
    }

    private void SpawnNpc()
    {
        Vector3 position = lastPosition;
        while(position == lastPosition) // prevent spawning 2 cars in row in the same position
        {
            position = GetRandomPosition();
        }
        lastPosition = position;

        Vector3 targetPosition = position;
        while(!IsValidPosition(targetPosition, position)) // prevent car to go to start position
        {
            targetPosition = GetRandomPosition();
        }
        GameObject go = Instantiate(NpcAsset, position, GetCarRotation(position), carsPool);
        go.GetComponent<CarController>().StartCar(CarType.Npc, targetPosition);
    }

    private bool IsValidPosition(Vector3 targetPosition, Vector3 position)
    {
        if (targetPosition == position) return false;
        
        if(Mathf.Abs(position.x) > Mathf.Abs(position.z))
        {
            return position.x != targetPosition.x;
        }
        else
        {
            return position.z != targetPosition.z;
        }
    }

    private Quaternion GetCarRotation(Vector3 position)
    {
        if(position.z == quadrantValue)
        {
            return Quaternion.Euler(0, 180, 0);
        }
        else if(position.x == quadrantValue)
        {
            return Quaternion.Euler(0, -90, 0);
        }
        else if(position.x == -quadrantValue)
        {
            return Quaternion.Euler(0, 90, 0);
        }
        else
        {
            return Quaternion.identity;
        }
    }

    // Get a random position on each quadrant
    private Vector3 GetRandomPosition()
    {
        float x;
        float z;
        if(Random.Range(0, 10) < 5)
        {
            x = laneValue;
            z = quadrantValue;
        }
        else
        {
            x = quadrantValue;
            z = laneValue;
        }
        if(Random.Range(0, 100) < 50)
        {
            x *= -1;
        }
        if(Random.Range(0, 20) < 10)
        {
            z *= -1;
        }
        return new Vector3(x, 0, z);
    }
}
