using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum CarType
{
    Player = 1,
    Bot,
    Npc
}

public class CarController : MonoBehaviour
{
    private Camera cam;
    private RaycastHit hit;
    private static int LEFT_CLICK = 0;
    private string groundTag = "Ground";
    private string npcTag = "NPC";
    private CarType carType;
    private CarColor carColor;
    private Vector3 targetPosition;
    private float travelDistance = 20;
    private static int playerSpeedMultiplier = 3;
    private static int npcSpeedMultiplier = 2;
    private Transform moneyPool;
    private HudManager hudManager;
    private int score;
    private bool collisionDebounce = false;
    private int debounceTime = 2;

    void Awake()
    {
        targetPosition = transform.position;
        score = 0;
    }

    public void StartCar(CarType _carType, CarColor _carColor = CarColor.Yellow, Transform _moneyPool = null, HudManager _hudManager = null)
    {
        carType = _carType;
        cam = Camera.main;
        moneyPool = _moneyPool;
        hudManager = _hudManager;
        carColor = _carColor;

        if(carType == CarType.Npc)
        {
            if(Random.Range(0, 10) < 5)
            {
                travelDistance *= npcSpeedMultiplier;
            }
        }
        else
        {
            hudManager.ActivateScore(carColor);
            travelDistance *= playerSpeedMultiplier;
        }
    }

    public void StartCar(CarType _carType, Vector3 _targetPosition)
    {
        StartCar(_carType);
        MoveCarToPosition(_targetPosition);
        
    }

    void Update()
    {
        if(carType == CarType.Player && Input.GetMouseButton(LEFT_CLICK))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if(hit.collider.CompareTag(groundTag))
                {
                    MoveCarToPosition(hit.point);
                }
            }
        }
    }

    void FixedUpdate()
    {
        if(carType == CarType.Npc)
        {
            if(Vector3.Distance(transform.position, targetPosition) < 0.1)
            {
                Destroy(gameObject);
            }
        }

        if(targetPosition != transform.position)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, travelDistance * Time.deltaTime);
        }
        else if(carType == CarType.Bot)
        {
            MoveCarToPosition(GetNextTargetPosition());
        }
    }

    Vector3 GetNextTargetPosition()
    {
        Vector3 nextTargetPosition = targetPosition;
        float distance = Mathf.Infinity;
        float aux = 0;

        foreach (Transform money in moneyPool)
        {
            aux = Vector3.Distance(money.position, transform.position);
            if(aux < distance)
            {
                distance = aux;
                nextTargetPosition = money.position;
            }
            if(Random.Range(0, 10) < 5 && money.GetComponent<Money>().Value >= 10000)
            {
                return money.position;
            }
        }

        return nextTargetPosition;
    }


    public void MoveCarToPosition(Vector3 position)
    {
        targetPosition = position;
        transform.rotation = Quaternion.LookRotation((targetPosition - transform.position).normalized);
    }

    public void MoneyCollected(int value)
    {
        if(carType == CarType.Bot)
        {
            MoveCarToPosition(GetNextTargetPosition());
        }
        UpdateScore(value);
    }
    
    private void CollidedWithNpc()
    {
        CollisionDebounce();
        UpdateScore(-10000);
    }

    void UpdateScore(int value)
    {
        score += value;
        hudManager.UpdateScore(carColor, score);
    }

    void OnTriggerEnter(Collider collider)
    {
        if(carType == CarType.Npc) return;
        
        if(!collisionDebounce && collider.gameObject.tag == npcTag)
        {
            CollidedWithNpc();
        }
    }

    private IEnumerator CollisionDebounce()
    {
        collisionDebounce = true;
        yield return new WaitForSeconds(debounceTime);
        collisionDebounce = false;
    }
}
