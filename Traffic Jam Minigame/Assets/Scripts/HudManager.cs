using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timer; 
    [SerializeField] private GameManager gameManager; 
    [SerializeField] private List<TextMeshProUGUI> scoreTexts;
    [SerializeField] private TextMeshProUGUI winnerText;
    private GameState gameState;
    private List<int> scores;

    void Awake()
    {
        GameManager.OnGameStateChanged += GameManagerOnGameStateChanged;
        gameState = GameState.Play;

        scores = new List<int>();
        foreach (var item in scoreTexts)
        {
            scores.Add(0);
        }
    }

    void OnDestroy()
    {
        GameManager.OnGameStateChanged -= GameManagerOnGameStateChanged;
    }

    private void GameManagerOnGameStateChanged(GameState _gameState)
    {
        gameState = _gameState;

        if(gameState == GameState.End)
        {
            UpdateWinner();
        }
    }

    private void UpdateWinner()
    {
        int winner = 0;
        int bestScore = 0;
        for (int i = 0; i < scores.Count; i++)
        {
            if(scores[i] > bestScore)
            {
                winner = i;
                bestScore = scores[i];
            }
        }

        winnerText.text = System.Enum.GetName(typeof(CarColor), (CarColor)winner) + " player won!";

        winnerText.GetComponentInParent<Image>().color = scoreTexts[winner].GetComponentInParent<Image>().color;
    }

    void FixedUpdate()
    {
        if(gameState == GameState.Play)
        {
            timer.text = Mathf.RoundToInt(gameManager.Timer).ToString();
        }
    }

    public void UpdateScore(CarColor carColor, int score)
    {
        scoreTexts[(int)carColor].text = FormatNumber(score);
        scores[(int)carColor] = score;
    }

    public void ActivateScore(CarColor carColor)
    {
        scoreTexts[(int)carColor].transform.parent.gameObject.SetActive(true);
    }

    static string FormatNumber(int value) 
    {
        if (value >= 100000)
            return FormatNumber(value / 1000) + "K";

        if (value >= 1000)
            return (value / 1000D).ToString("0.#") + "K";

        return value.ToString("#,0");
    }
}
