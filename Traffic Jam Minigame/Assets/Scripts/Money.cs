using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{
    [SerializeField] private int value;
    private string playerTag = "Player";
    
    public int Value => value;

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == playerTag)
        {
            collider.gameObject.GetComponent<CarController>().MoneyCollected(value);
            Destroy(gameObject);
        }
    }
}
